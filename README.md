# Element_blg

Ceci est un projet de fin d'étude, pour valider le passage du titre professionnelle de niveau III, de "Développeur web et web mobile".

Il s'agit d'un blog communautaire, sur les les site terrestre des  element de  l'eau, la terre, le ciel.

Chaque utilisateur inscrit pourras crée un article. Il pourras avoir des favories des articles des autres utilisateurs. Il pourras commenter et liké les posts qu'il choisi.

Les technologies utilisés pour la réalisation de cé projet sont les suivantes:

en back-end
- PHP 7.2.4 et son framework Symfony
- SQL pour le langage de BD avec son gestionnaire MariaDB
- ORM Doctrine

en front
- Angular JS 7