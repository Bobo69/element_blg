<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191115152221 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users_favorie (users_id INT NOT NULL, favorie_id INT NOT NULL, INDEX IDX_8DE705A967B3B43D (users_id), INDEX IDX_8DE705A9249A8F58 (favorie_id), PRIMARY KEY(users_id, favorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_favorie ADD CONSTRAINT FK_8DE705A967B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_favorie ADD CONSTRAINT FK_8DE705A9249A8F58 FOREIGN KEY (favorie_id) REFERENCES favorie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE articles ADD author_id INT NOT NULL');
        $this->addSql('ALTER TABLE articles ADD CONSTRAINT FK_BFDD3168F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_BFDD3168F675F31B ON articles (author_id)');
        $this->addSql('ALTER TABLE commentary ADD author_id INT NOT NULL, ADD articles_id INT NOT NULL');
        $this->addSql('ALTER TABLE commentary ADD CONSTRAINT FK_1CAC12CAF675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE commentary ADD CONSTRAINT FK_1CAC12CA1EBAF6CC FOREIGN KEY (articles_id) REFERENCES articles (id)');
        $this->addSql('CREATE INDEX IDX_1CAC12CAF675F31B ON commentary (author_id)');
        $this->addSql('CREATE INDEX IDX_1CAC12CA1EBAF6CC ON commentary (articles_id)');
        $this->addSql('ALTER TABLE categorie ADD articles_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE categorie ADD CONSTRAINT FK_497DD6341EBAF6CC FOREIGN KEY (articles_id) REFERENCES articles (id)');
        $this->addSql('CREATE INDEX IDX_497DD6341EBAF6CC ON categorie (articles_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE users_favorie');
        $this->addSql('ALTER TABLE articles DROP FOREIGN KEY FK_BFDD3168F675F31B');
        $this->addSql('DROP INDEX IDX_BFDD3168F675F31B ON articles');
        $this->addSql('ALTER TABLE articles DROP author_id');
        $this->addSql('ALTER TABLE categorie DROP FOREIGN KEY FK_497DD6341EBAF6CC');
        $this->addSql('DROP INDEX IDX_497DD6341EBAF6CC ON categorie');
        $this->addSql('ALTER TABLE categorie DROP articles_id');
        $this->addSql('ALTER TABLE commentary DROP FOREIGN KEY FK_1CAC12CAF675F31B');
        $this->addSql('ALTER TABLE commentary DROP FOREIGN KEY FK_1CAC12CA1EBAF6CC');
        $this->addSql('DROP INDEX IDX_1CAC12CAF675F31B ON commentary');
        $this->addSql('DROP INDEX IDX_1CAC12CA1EBAF6CC ON commentary');
        $this->addSql('ALTER TABLE commentary DROP author_id, DROP articles_id');
    }
}
