<?php

namespace App\DataFixtures;

use App\Entity\Articles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++){
            $article = new Articles();
            $article -> setTitle('article'.$i);
            $article -> setContent('BloupBlip'.$i);
            $manager -> persist($article);

        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
